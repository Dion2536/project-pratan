<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
$nameWeb = 'ศิลาทราย OTOP';
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>

<div class="wrap">
    <!--    --><?php
    //    NavBar::begin([
    //        'brandLabel' => Yii::$app->name,
    //        'brandUrl' => Yii::$app->homeUrl,
    //        'options' => [
    //            'class' => 'navbar navbar-default navbar-fixed-top',
    //        ],
    //    ]);
    //     Nav::widget([
    //        'options' => ['class' => 'navbar-nav navbar-right'],
    //        'items' => [
    //            ['label' => 'Home', 'url' => ['/site/index']],
    //            ['label' => 'About', 'url' => ['/site/about']],
    //            ['label' => 'Contact', 'url' => ['/site/contact']],
    //            Yii::$app->user->isGuest ? (
    //                ['label' => 'Login', 'url' => ['/site/login']]
    //            ) : (
    //                '<li>'
    //                . Html::beginForm(['/site/logout'], 'post')
    //                . Html::submitButton(
    //                    'Logout (' . Yii::$app->user->identity->username . ')',
    //                    ['class' => 'btn btn-link logout']
    //                )
    //                . Html::endForm()
    //                . '</li>'
    //            )
    //        ],
    //    ]);
    //    NavBar::end();
    //    ?>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand visible-xs" href="#"><?= $nameWeb ?></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <?php echo Nav::widget([
                    'options' => ['class' => 'nav navbar-nav nav-left'],
                    'items' => [
                        ['label' => 'หน้าหลัก', 'url' => ['/site/index']],
                        ['label' => 'สินค้าทั้งหมด', 'url' => ['/site/product']],
                        ['label' => 'กิจกรรม', 'url' => ['/site/product']],
                        ['label' => 'ติดต่อเรา', 'url' => ['/site/product']],
                        ['label' => 'สั่งทำสินค้า', 'url' => ['/site/contact']]
                    ],
                ]); ?>
                <a href="#" class="logo visible-lg visible-md"><img
                            src="<?= Yii::$app->request->baseUrl . '/img/logo1.jpg' ?>"
                            alt="dodolan manuk responsive catalog themes"></a>
                <div id="brand" class="visible-lg visible-md">&nbsp;</div>
                <?php
                if (Yii::$app->user->isGuest) {
                    $item = [
                        ['label' => 'ตะกร้าสินค้า (0)', 'url' => ['/site/order-product']],
                        ['label' => 'สมัครสมาชิก', 'url' => ['/site/contact']],
                        ['label' => 'เข้าสู่ระบบ', 'url' => ['/site/login']],
                    ];
                } else {
                    $item = [
                        ['label' => 'ตะกร้าสินค้า (0)', 'url' => ['/site/order-product']],
                        ['label' => 'ข้อมูลผู้ใช้งาน', 'url' => ['/site/login']],
                        '<li>'
                        . Html::beginForm(['/site/logout'], 'post')
                        . Html::submitButton(
                            'ออกจากระบบ (' . Yii::$app->user->identity->username . ')',
                            ['class' => 'btn btn-link logout']
                        )
                        . Html::endForm()
                        . '</li>'];
                }
                ?>
                <?php echo Nav::widget([
                    'options' => ['class' => 'nav navbar-nav nav-right'],
                    'items' => $item,
                    'encodeLabels' => false
                ]); ?>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <div class="">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
        <div class="main_section">
            <div class="">
                <div class="chat_container">
                    <div class="col-sm-12 message_section">
                        <div class="row">
                            <div class="new_message_head">
                                <div class="pull-left">
                                    <button><i class="fa fa-plus-square-o" aria-hidden="true"></i> New Message</button>
                                </div>
                                <div class="pull-right">
                                    <div class="dropdown">
                                        <button class="dropdown-toggle" type="button" id="dropdownMenu1"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-cogs" aria-hidden="true"></i> Setting
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Profile</a></li>
                                            <li><a href="#">Logout</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div><!--new_message_head-->

                            <div class="chat_area">
                                <ul class="list-unstyled">
                                    <li class="left clearfix">
                     <span class="chat-img1 pull-left">
                         <i class="fa fa-user-md fa-4x"></i>
                     </span>
                                        <div class="chat-body1 clearfix">
                                            <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has
                                                roots in a piece of classical Latin literature from 45 BC, making it
                                                over 2000 years old. Richard McClintock, a Latin professor at
                                                Hampden-Sydney College in Virginia.</p>
                                            <div class="chat_time pull-left">09:40PM</div>
                                        </div>
                                    </li>


                                    <li class="right clearfix admin_chat">
                     <span class="chat-img1 pull-right">

                     </span>
                                        <div class="chat-body2 clearfix">
                                            <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has
                                                roots in a piece of classical Latin literature from 45 BC, making it
                                                over 2000 years old. Richard McClintock, a Latin professor at
                                                Hampden-Sydney College in Virginia.</p>
                                            <div class="chat_time pull-right">09:40PM</div>
                                        </div>
                                    </li>


                                </ul>
                            </div><!--chat_area-->
                            <div class="message_write">
                                <textarea class="form-control" placeholder="type a message"></textarea>
                                <div class="clearfix"></div>
                                <div class="chat_bottom"><a href="#" class="pull-left upload_btn"><i
                                                class="fa fa-cloud-upload" aria-hidden="true"></i>
                                        Add Files</a>
                                    <a href="#" class="pull-right btn btn-success">
                                        Send</a></div>
                            </div>
                        </div>
                    </div> <!--message_section-->
                </div>
            </div>
        </div>
        <div class="bt-ms bt-sm-app">
            <button class="btn btn-danger btn-lg">
                <i class="glyphicon glyphicon-comment"></i> พูดคุยกับทางร้าน
            </button>
        </div>

    </div>
</div>

<!-- begin:footer -->
<div id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3><?= $nameWeb ?></h3>
                <p>
                    รับปั้นรูปเหมือนทุกประเภท งานหล่อไฟเบอร์กลาส งานเเกะต่างๆ</p>


            </div>
        </div>
    </div>
</div>
<!-- end:footer -->

<!-- begin:copyright -->
<div id="copyright">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <small>Copyright &copy; <?= Date('Y') ?> <?= $nameWeb ?> All Right Reserved.
                </small>
            </div>
        </div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
