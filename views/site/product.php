<?php
?>
<div class="heads" style="background: url(<?=Yii::$app->request->baseUrl .'/img/img01-bg.png' ?>) center center;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><span>//</span> รายการสินค้าทั้งหมด</h2>
            </div>
        </div>
</div>
</div>

<div id="catalogue">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading-title">
                    <h2>รายการสินค้างานปั้นต่างๆ</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <?php foreach ($data as $model):?>
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="thumbnail">
                    <div class="caption-img" style="background: url(<?=Yii::$app->request->baseUrl.'/img/product/'.$model['img']?>);"></div>
                    <div class="caption-details">
                        <h3><?=$model['name']?></h3>
                        <span class="price"><?=$model['price']?> บ.</span>
                    </div>
                    <a href="detail.html">
                        <div class="caption-link"><i class="fa fa-plus"></i></div>
                    </a>
                </div>
            </div>
            <?php endforeach;?>

        </div>
    </div>
</div>