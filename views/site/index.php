<?php

/* @var $this yii\web\View */

$this->title = 'หน้าหลัก';
?>
<div id="home">
    <div id="home-slider" class="carousel slide carousel-fade" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#home-slider" data-slide-to="0" class="active"></li>
            <li data-target="#home-slider" data-slide-to="1"></li>
            <li data-target="#home-slider" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="item active" style="background: url(<?=Yii::$app->request->baseUrl .'/img/่jj.jpg' ?>);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="carousel-content">
                                <!-- <h2 class="animated fadeInUpBig text-center text-black">ร้านศิลาทราย ยินดีต้อนรับ</h2> -->
                                <!-- <p class="animated rollIn text-black text-center"><span class="text900">Welcome to Dodolan Manuk</span> -->
                                    <!-- an awesome catalog theme, <br> built with <i class="fa fa-heart-o"></i> in <span -->
                                            <!-- class="text900">Ngayogyokarto hadiningrat.</span><br><br> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="item" style="background: url(img/img02.jpg);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="carousel-content">
                                <h3 class="animated fadeInLeftBig text-left">.Built</h3>
                                <p class="animated fadeInDownBig text-left">Your own online shop using this catalog
                                    theme.<br> Simple and easy cutomize just $12.</p>
                                <a class="btn btn-green btn-lg animated fadeInRight" href="#">Learn more &raquo;</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a class="left carousel-control" href="#home-slider" data-slide="prev">
            <i class="fa fa-angle-left"></i>
        </a>
        <a class="right carousel-control" href="#home-slider" data-slide="next">
            <i class="fa fa-angle-right"></i>
        </a>
        <div class="pattern"></div>
    </div>

</div>

<div id="tagline">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>ศิลาทราย OTOP ยินดีต้อนรับ</h2>
                <p>รับปั้นรูปเหมือนทุกประเภท งานหล่อไฟเบอร์กลาส งานเเกะต่างๆ สามารถสั่งซื้อสินค้าเราได้
                    หรือลูกค้าท่านอยากปั้นตามใจท่านสามารถกดสั่งทำได้</p>
            </div>
        </div>
    </div>
</div>

<?=$this->render('product',["data"=>$data])?>

