<?php

namespace app\modules\admin\controllers;

use yii\data\ArrayDataProvider;

class OrderController extends \yii\web\Controller
{
    public  $layout ='main';
    public function actionIndex()
    {
        $data = [
            ["ผู้สั่ง"=>"user1","ชื่อสินค้า" => "สินค้าที่ 1 ", "จำนวน" => "2","ราคา"=>"2,510", "วันที่" =>"20-06-2559"],
            ["ผู้สั่ง"=>"user2","ชื่อสินค้า" => "สินค้าที่ 2 ", "จำนวน" => "3","ราคา"=>"1,510", "วันที่" =>"10-06-2559"],

        ];

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => [
                'pageSize' => 3,
            ],
            'sort' => [
                'attributes' => ['ผู้สั่ง', 'วันที่'],
            ],
        ]);

        return $this->render('index',['dataProvider'=>$dataProvider]);
    }

}
