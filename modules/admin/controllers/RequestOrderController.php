<?php

namespace app\modules\admin\controllers;


use yii\data\ArrayDataProvider;

class RequestOrderController extends \yii\web\Controller
{
    public  $layout ='main';
    public function actionIndex()
    {
        $data = [
            ["fastname" => "นาย1", "lastname" => "สกุล1", "phone" =>"0812345671","sex"=>"ชาย" ],
            ["fastname" => "นาย2", "lastname" => "สกุล2", "phone" =>"0812345672","sex"=>"ชาย" ],
            ["fastname" => "นาย3", "lastname" => "สกุล3", "phone" =>"0812345673","sex"=>"ชาย" ],
            ["fastname" => "นาง1", "lastname" => "สกุล2", "phone" =>"0812345672","sex"=>"หญิง" ],
            ["fastname" => "นาง2", "lastname" => "สกุล3", "phone" =>"0812345672","sex"=>"หญิง" ],
        ];

        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => [
                'pageSize' => 3,
            ],
            'sort' => [
                'attributes' => ['fastname', 'lastname'],
            ],
        ]);

        return $this->render('index',['dataProvider'=>$dataProvider]);
    }

}
